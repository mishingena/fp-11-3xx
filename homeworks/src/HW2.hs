-- Тесты чуть позже

module HW2
       ( Contact (..)
       , isKnown
       , Term' (..)
       , eval_
       , simplify
       ) where

data Contact = On
             | Off
             | Unknown

isKnown :: Contact -> Bool
isKnown Unknown = False
isKnown _ = True

data Term' = Mult Term' Term'      -- умножение
          | Add Term' Term'       -- сложение
          | Sub Term' Term'       -- вычитание
          | Const Int           -- константа

eval_ :: Term' -> Int
eval_ (Const a) = a
eval_ (Sub term1 term2) = (eval_ term1) - (eval_ term2)
eval_ (Add term1 term2) = (eval_ term1) + (eval_ term2)
eval_ (Mult term1 term2) = (eval_ term1) * (eval_ term2)

term1 = Const 10
term2 = Add term1 term1
term3 = Add term1 term2
term4 = Sub term3 term2
term5 = Mult term4 term1
term6 = Sub term1 term2
term7 = Mult term1 term1
term8 = Mult term1 term7
term9 = Mult term1 term2

-- Раскрыть скобки
-- Mult (Add (Const 1) (Const 2)) (Const 3) ->
-- Add (Mult (Const 1) (Const 3)) (Mult (Const 2) (Const 3))
-- (1+2)*3 -> 1*3+2*3
termString :: Term' -> String
termString (Const a) = show a
termString (Add term1 term2) = termString term1 ++ "+" ++ termString term2
termString (Sub term1 term2) = termString term1 ++ "-" ++ termString term2
termString (Mult term1 term2) = termString term1 ++ "*" ++ termString term2

negative :: Term' -> Term'
negative (Const a) = Const (-a)
negative (Add a b) = Add (negative a) (negative b)
negative (Sub a b) = Add (negative a) b
negative (Mult a b)  = Mult (negative a) b

multiply :: Term' -> Term' -> Term'
multiply (Const a) (Const b) = Mult (Const a) (Const b)
multiply (Const a) (Add term1 term2) = Add (multiply term1 (Const a)) (multiply term2 (Const a))
multiply (Add term1 term2) (Const a) = multiply (Const a) (Add term1 term2)
multiply (Const a) (Sub term1 term2) = Sub (multiply term1 (Const a)) (multiply term2 (Const a))
multiply (Sub term1 term2) (Const a) = multiply (Const a) (Sub term1 term2)

simplify :: Term' -> Term'
simplify (Const a) = Const a
simplify (Add term1 term2) = Add (simplify term1) (simplify term2)
simplify (Sub term1 term2) = Add (simplify term1) (simplify (negative term2))
simplify (Mult term1 term2) = Add (Mult (simplify term1) term2) (Mult (simplify term2) term1)
--simplify (Mult term1 term2) = Mult (simplify term1) (simplify term2)
--simplify = error "simplify"

