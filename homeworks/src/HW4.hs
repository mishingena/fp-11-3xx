-- Срок: 2016-04-02 (100%), 2016-04-07 (50%)

module HW4
       ( Show' (..)
       , A(..)
       , C(..)
       , Set(..)
       , symmetricDifference
       , fromBool
       , fromInt
       ) where

class Show' a where
  show' :: a -> String

data A = A Int
       | B
-- show' (A 5)    == "A 5"
-- show' (A (-5)) == "A (-5)"
-- show' B        == "B"
instance Show' A where
    show' (A int) = "A " ++ numToStr int
    show' B = "B"

data C = C Int Int
-- show' (C 1 2)       == "C 1 2"
-- show' (C (-1) (-2)) == "C (-1) (-2)"
instance Show' C where
    show' (C a b) = "C " ++ numToStr a ++ " " ++ numToStr b

numToStr int = if int < 0 then "(" ++ show int ++ ")" else show int

--instance Show' A where
--  show' = undefined
--
--instance Show' C where
--  show' = undefined

----------------

data Set a = Set (a -> Bool)

-- Симметрическая разность -
-- элементы обоих множеств, не входящие в объединение
-- {4,5,6} \/ {5,6,7,8} == {4,7,8}
symmetricDifference :: Set a -> Set a -> Set a
symmetricDifference (Set f1) (Set f2) = Set $ \elem -> let elem_in_f1 = f1 elem
                                                           elem_in_f2 = f2 elem
                      in (elem_in_f1 || elem_in_f2) && not (elem_in_f1 && elem_in_f2)

-----------------

tru = \t -> (\f -> t)
fls = \t f -> f

-- fromBool - переводит булевское значение в кодировку Чёрча
fromBool bool
    | bool = tru
    | otherwise = fls

-- fromInt - переводит число в кодировку Чёрча
fromInt n
    | n == 0 = \s z -> z
    | otherwise = \s z -> s (fromInt (n - 1) s z)

