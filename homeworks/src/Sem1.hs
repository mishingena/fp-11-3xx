module Sem1 where

import Data.Char

data Term = Var String
        | Lambda String Term
        | Apply Term Term
        deriving Eq

instance Show Term where
    show (Var v) = v
    show (Lambda v t) = "(\\" ++ v ++ " . " ++ show t ++ ")"
    show (Apply t1 t2) = show t1 ++ " " ++ show t2

--Вычисление терма на один шаг
eval_1 :: Term -> Maybe Term
eval_1 term@(Apply l1@(Lambda (v1) t1) l2) = Just (replaceVar v1 l2 t1 (getUsedVars t1))
--Сначала вычисляем самый внутренний (т.е. в случае (((l1 a1) a2) a3) a4 вычисляем l1 a1)
--apply слева
eval_1 term@(Apply a1@(Apply t1 t2) t3) = Just $ Apply eval_1_a1 t3
    where (Just eval_1_a1) = eval_1 a1
--apply справа
eval_1 term@(Apply t1 a1@(Apply t2 t3)) = Just $ Apply t1 eval_1_a1
    where (Just eval_1_a1) = eval_1 a1
eval_1 term = Just term


--Вычисление терма на много шагов
--eval :: Term -> Term
--eval term@(Apply v1@(Var v) t@(Apply t2 t3)) = Apply v1 (eval t)
--eval term@(Apply a1@(Apply (Var v) t2) t3) = Apply a1 (eval t3)
--eval term@(Apply a1@(Apply t1 t2) t3) = eval $ Apply (eval a1) t3
--eval term@(Apply l1@(Lambda (v1) t1) l2) = eval $ replaceVar v1 l2 t1 (getUsedVars t1)
--eval term@(Apply t1 a1@(Apply (Var v) t2)) = Apply (eval t1) a1
--eval term@(Apply t1 a1@(Apply t2 t3)) = eval $ Apply t1 (eval a1)
--eval term = term

eval :: Term -> Term
eval term@(Apply v1@(Var v) t@(Apply t2 t3)) = Apply v1 (eval t)
eval term@(Apply a1@(Apply (Var v) t2) t3) = Apply a1 (eval t3)
eval term@(Apply a1@(Apply t1 t2) t3) = eval $ Apply (eval a1) t3
eval term@(Apply l1@(Lambda (v1) t1) l2) = eval $ replaceVar v1 l2 t1 (getUsedVars t1)
eval term@(Apply t1 a1@(Apply (Var v) t2)) = Apply (eval t1) a1
eval term@(Apply t1 a1@(Apply t2 t3)) = eval $ Apply t1 (eval a1)
eval term = term



--Заменить все свободные вхождения переменной var на терм term в терме inTerm_
--usedVars - занятые переменные, которых не должно быть в итоговом выражении
replaceVar :: String -> Term -> Term -> [String] -> Term
replaceVar var term inTerm_ usedVars = case inTerm_ of
    Var v -> if v == var then term else Var v
    lam@(Lambda (v) (Var v1)) -> if v /= v1 && v1 == var
                    then Lambda (v) new_term --лямбду не упрощаем, просто заменяем свободное вхождение
                    else lam
    lam@(Lambda (v) (Lambda v2 t)) -> if v == var  --Если лямбда имеет в качестве параметра пер-ную с таким же именем("перекрытие")
                    then lam
                    else Lambda (v) (replaceVar var new_term (Lambda v2 t) usedVars)
    lam@(Lambda (v) (Apply t1 t2)) -> if v == var
                    then lam
                    else Lambda (v) (replaceVar var new_term (Apply t1 t2) usedVars)
    app@(Apply t1 t2) -> Apply (replaceVar var new_term t1 usedVars) (replaceVar var new_term t2 usedVars)
    where new_term = case term of  --Заменяем имя переменной на свободное, если она связана в inTerm
                Var v -> if v `elem` usedVars then Var (replaceOnFreeVar usedVars "a") else Var v
                _ -> replaceFreeVars (getUsedVars term ++ usedVars) usedVars term

--в терме term заменить все вхождения переменных, имеющих такие же названия в usedVars, на уникальные
replaceFreeVars :: [String] -> [String] -> Term -> Term
replaceFreeVars _ [] term = term
replaceFreeVars usedVarsFull usedVars@(x:xs) term = replaceFreeVars (newVar : usedVarsFull) xs newTerm
    where (newTerm, newVar) = replaceFreeVar x term usedVarsFull

--Заменить в терме term переменную usedVar на свободную (т.е. не на одну из usedVars).
replaceFreeVar :: String -> Term -> [String] -> (Term, String)
replaceFreeVar usedVar term usedVars = case term of
        Var v -> if usedVar == v
            then (Var onVar, onVar)
            else (Var v, "-1")  --v - свободная переменная
        l@(Lambda (v) t) -> if usedVar == v
                then (Lambda (onVar) (forceReplaceVar v onVar t), onVar)
                else let (newTerm, newVar) = replaceFreeVar usedVar t usedVars
                        in (Lambda (v) newTerm, newVar)
        Apply t1 t2 -> let (newTerm1, newVar1) = replaceFreeVar usedVar t1 usedVars
                           (newTerm2, newVar2) = replaceFreeVar usedVar t2 usedVars
                          in (Apply newTerm1 newTerm2, newVar1)
    where onVar = replaceOnFreeVar usedVars "a"

--Заменить в терме term все встречаемости переменной var на переменную onVar
forceReplaceVar :: String -> String -> Term -> Term
forceReplaceVar var onVar term = case term of
    Var v -> if v == var then Var onVar else Var v
    Lambda (v) t -> if v == var then error "Damn" else Lambda (v) (forceReplaceVar var onVar t)
    Apply t1 t2 -> Apply (forceReplaceVar var onVar t1) (forceReplaceVar var onVar t2)

--Получить все связанные переменные из терма
getUsedVars :: Term -> [String]
getUsedVars term = case term of
    Lambda (v) t -> v : getUsedVars t
    Apply t1 t2 -> getUsedVars t1 ++ getUsedVars t2
    (Var v) -> []

--Замена переменной var на первую переменную, которая не входит в usedVars
--В качестве переменных берем буквы английского алфавита
replaceOnFreeVar :: [String] -> String -> String
replaceOnFreeVar usedVars var = if var `elem` usedVars
                            then replaceOnFreeVar usedVars (nextVar var)
                            else var
                 where nextVar [ch] = [chr (ord ch + 1)]
