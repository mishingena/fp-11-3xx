module HW1
       ( hw1_1
       , hw1_2
       , fact2
       , isPrime
       , primeSum
       ) where

-- Запускать при помощи: haskellstack.org
-- stack setup - установка GHC нужной версии и т.д.
-- stack build - компиляция
-- stack test  - тесты

-- |Вычислить сумму двух аргументов
hw1_1 :: Integer -> Integer -> Integer
hw1_1 a b = a + b

-- |Вычислить сумму N членов ряда
--
--
--  N
-- ---
-- \    1
--  >  ---
-- /   k^k
-- ---
-- k=1
--
-- Использовать fromIntegral для перевода из Integer в Double
hw1_2 :: Integer -> Double
hw1_2 n = fromIntegral n

-- |Вычислить двойной факториал
-- n!! = 1*3*5*...*n, если n - нечетное
-- n!! = 2*4*6*...*n, если n - четное
fact2 :: Integer -> Integer
fact2 2 = 2
fact2 1 = 1
fact2 0 = 1
fact2 n = n * fact2 (n-2)


-- |Проверить заданное число на простоту
-- Использовать div для целочисленного деления
-- или mod для остатка от деления
isPrime :: Integer -> Bool
isPrime 1 = True
isPrime 2 = True
isPrime n = loop n ((truncate . sqrt . fromIntegral $ n) + 1)

loop :: Integer -> Integer -> Bool
loop 2 _ = True
loop 1 _ = True
loop n 1 = True
loop n m = if mod n m == 0 then False else loop n (m-1)


-- |Найти сумму всех простых чисел в диапазоне [a;b]
primeSum :: Integer -> Integer -> Integer
primeSum a b = loop2 a b 0

loop2 :: Integer -> Integer -> Integer -> Integer
--loop2 a b i sum
loop2 a b sum = if a > b then sum else if isPrime a then loop2 (a+1) b (sum + a) else loop2 (a+1) b sum
