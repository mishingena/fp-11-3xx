-- Do not change! It will be overwritten!
-- Use MyTests.hs to define your tests

import Test.Tasty (TestTree(..), defaultMainWithIngredients, testGroup)
import Test.Tasty.Ingredients.Basic (consoleTestReporter, listingTests)
import Test.Tasty.Runners.AntXML (antXMLRunner)

--import AutoTests (exampleTests, oldTests, autoTests)
import MyTests (myTests)

main :: IO ()
main = defaultMainWithIngredients [antXMLRunner, consoleTestReporter, listingTests] tests

tests :: TestTree
tests = testGroup "Tests"
        [
--        testGroup "Example tests" exampleTests
         testGroup "User defined tests" myTests
--        , testGroup "Old tests" oldTests
--        , testGroup "Auto tests" autoTests
        ]

