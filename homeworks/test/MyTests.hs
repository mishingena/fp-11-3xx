-- Your tests go here

module MyTests where

import Test.Tasty (TestTree(..), testGroup)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

myTests :: [TestTree]
myTests = [ testGroup "HW0"
            [ testCase "Works on Alice" $ hw0_0 "Alice" @?= "Hello, Alice" ],
            testGroup "Sem1 eval_1" [
                                   testCase "eval_1 (\\x . x) y == y" $ eval_1 (Apply (Lambda ("x") (Var "x")) (Var "y")) @?= Just (Var "y"),

                                   testCase "eval_1 (\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                            $ eval_1 (Apply (Lambda ("x")
                                                                   (Lambda ("y") (Apply (Var "x") (Var "y"))))
                                                           (Lambda ("z") (Var "z")))
                                                    @?= Just (Lambda ("y") (Apply (Lambda ("z") (Var "z")) (Var "y"))),

                                   testCase "eval_1 (\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\y . (\\z . z) y) (\\u . \\v . v)"
                                            $ show (eval_1 (Apply (Apply (Lambda ("x")
                                                                   (Lambda ("y") (Apply (Var "x") (Var "y"))))
                                                           (Lambda ("z") (Var "z")))
                                                        (Lambda ("u") (Lambda ("v") (Var "v")))))
                                                    @?= "Just (\\y . (\\z . z) y) (\\u . (\\v . v))",

                                   testCase "eval_1 (\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                                            $ show (eval_1 (Lambda ("x") (Apply (Lambda ("x") (Var "x")) (Var "x"))))
                                                @?= "Just (\\x . (\\x . x) x)",

                                   testCase "eval_1 (\\x . \\y . x) y = (\\y . a)"
                                            $ show (eval_1 (Apply (Lambda ("x") (Lambda ("y") (Var "x"))) (Var "y")))
                                                @?= "Just (\\y . a)",

                                   testCase "eval_1 (\\y. \\x . y) (\\y . x) = (\\x . (\\y . a))"
                                            $ eval_1 (Apply (Lambda ("y") (Lambda ("x") (Var "y"))) (Lambda ("y") (Var "x")))
                                                @?= Just (Lambda ("x") (Lambda ("y") (Var "a"))),

                                   testCase "eval_1 (\\x. \\y . x) (\\y . y) = (\\y . (\\a . a))"
                                            $ eval_1 (Apply (Lambda ("x") (Lambda ("y") (Var "x"))) (Lambda ("y") (Var "y")))
                                                @?= Just (Lambda ("y") (Lambda ("a") (Var "a"))),

                                   testCase "eval_1 (\\y . (\\y . x y) y) z = (\\y . x y) z"
                                            $ eval_1 (Apply (Lambda ("y")
                                                            (Apply (Lambda ("y") (Apply (Var "x") (Var "y"))) (Var "y"))) (Var "z"))
                                                @?= Just (Apply (Lambda ("y") (Apply (Var "x") (Var "y"))) (Var "z"))
                             ],
                        testGroup "Sem1 eval_" [
                                   testCase "eval(\\x . x) y == y" $ eval(Apply (Lambda ("x") (Var "x")) (Var "y")) @?= Var "y",

                                   testCase "eval(\\x . \\y . x y) (\\z . z) == (\\y . (\\z . z) y)"
                                            $ eval(Apply (Lambda ("x")
                                                                   (Lambda ("y") (Apply (Var "x") (Var "y"))))
                                                           (Lambda ("z") (Var "z")))
                                                    @?= Lambda ("y") (Apply (Lambda ("z") (Var "z")) (Var "y")),

                                   testCase "eval(\\x . \\y . x y) (\\z . z) (\\u . \\v . v) == (\\u . \\v . v)"
                                            $ eval(Apply (Apply (Lambda ("x")
                                                                   (Lambda ("y") (Apply (Var "x") (Var "y"))))
                                                           (Lambda ("z") (Var "z")))
                                                        (Lambda ("u") (Lambda ("v") (Var "v"))))
                                                    @?= Lambda ("u") (Lambda ("v") (Var "v")),

                                   testCase "eval(\\x . (\\x . x) x) == (\\x . (\\x . x) x)"
                                            $ show (eval(Lambda ("x") (Apply (Lambda ("x") (Var "x")) (Var "x"))))
                                                @?= "(\\x . (\\x . x) x)",

                                   testCase "eval(\\x . \\y . x) y = (\\y . a)"
                                            $ show (eval(Apply (Lambda ("x") (Lambda ("y") (Var "x"))) (Var "y")))
                                                @?= "(\\y . a)",

                                   testCase "eval(\\x . \\a . x a) a = (\\a . b a)"
                                            $ show (eval(Apply (Lambda ("x")
                                                        (Lambda ("a") (Apply (Var "x") (Var "a")))) (Var "a")))
                                                @?= "(\\a . b a)",

                                   testCase "eval(\\y. \\x . y) (\\y . x) = (\\x . (\\y . a))"
                                            $ eval(Apply (Lambda ("y") (Lambda ("x") (Var "y"))) (Lambda ("y") (Var "x")))
                                                @?= Lambda ("x") (Lambda ("y") (Var "a")),

                                   testCase "eval(\\x. \\y . x) (\\y . y) = (\\y . (\\a . a))"
                                            $ eval(Apply (Lambda ("x") (Lambda ("y") (Var "x"))) (Lambda ("y") (Var "y")))
                                                @?= Lambda ("y") (Lambda ("a") (Var "a")),

                                   testCase "eval(\\a . \\x . (\\b . b y) a x) (\\a . \\b . b z) = (\\x . (\\b . b y) (\\a . \\c . c z) x)"
                                            $ show (eval(Apply (Lambda ("a") (Lambda ("x")
                                            (Apply (Apply (Lambda ("b") (Apply (Var "b") (Var "y"))) (Var "a")) (Var "x"))))
                                            (Lambda ("a") (Lambda ("b") (Apply (Var "b") (Var "z"))))))
                                                 @?= "(\\x . (\\b . b y) (\\a . (\\c . c z)) x)",

                                   testCase "eval(\\a . \\x . b a x) (\\a . \\b . b z) = (\\x . b (\\a . \\b . b z) x)"
                                              $ eval(Apply
                                                        (Lambda ("a") (Lambda ("x") (Apply (Var "b") (Apply (Var "a") (Var "x")))))
                                                        (Lambda ("a") (Lambda ("b") (Apply (Var "b") (Var "z")))))
                                                @?= Lambda ("x") (Apply (Var "b") (Apply
                                                                        (Lambda ("a") (Lambda ("b") (Apply (Var "b") (Var "z"))))
                                                                        (Var "x"))),

                                   testCase "eval(\\y . \\a . y a) (\\a . a y) = (\\a . (\\b . b y) a)"
                                            $ eval(Apply
                                                        (Lambda ("y") (Lambda ("a") (Apply (Var "y") (Var "a"))))
                                                        (Lambda ("a") (Apply (Var "a") (Var "y"))))
                                                @?= Lambda ("a") (Apply (Lambda ("b") (Apply (Var "b") (Var "y"))) (Var "a")),

                                   testCase "eval(\\x . \\y . x x y) (\\a . a y) b = c c b"
                                            $ show (eval(Apply
                                                        (Apply (Lambda ("x") (Lambda ("y") (Apply (Apply (Var "x") (Var "x")) (Var "y"))))
                                                                (Lambda ("a") (Apply (Var "a") (Var "y"))))
                                                        (Var "b")))
                                                @?= "b b b",
                                   testCase "eval(\\m . \\s . \\z . m s z) (\\s . \\z . s z)"
                                            $ show (eval
                                                        (Apply (Lambda ("m")
                                                                            (Lambda ("s")
                                                                                (Lambda ("z")
                                                                                    (Apply (Apply (Var "m") (Var "s")) (Var "z")))))
                                                                (Lambda ("s") (Lambda ("z") (Apply (Var "s") (Var "z"))))))
                                                        @?= "(\\s . (\\z . (\\a . (\\b . a b)) s z))",
                                   testCase "eval(\\m . \\n . \\s . \\z . m (n s) z) (\\s . \\z . s z) (\\s . \\z . s z) S Z"
                                            $ show (eval(Apply
                                                    (Apply (
                                                        Apply (Apply (Lambda ("m")
                                                                        (Lambda ("n")
                                                                            (Lambda ("s")
                                                                                (Lambda ("z")
                                                                                    (Apply (Apply (Var "m") (Apply (Var "n") (Var "s"))) (Var "z"))))))
                                                                (Lambda ("s") (Lambda ("z") (Apply (Var "s") (Var "z")))))
                                                            (Lambda ("s") (Lambda ("z") (Apply (Var "s") (Var "z")))))
                                                        (Var "S"))
                                                    (Var "Z")))
                                                        @?= "S Z",
                                   testCase "eval (\\m . \\n . \\s . \\z . m (n s) z) (\\s . \\z . s (s z)) (\\s . \\z . s z) S Z"
                                                                   $ eval (Apply
                                                                           (Apply (
                                                                               Apply (Apply (Lambda ("m")
                                                                                               (Lambda ("n")
                                                                                                   (Lambda ("s")
                                                                                                       (Lambda ("z")
                                                                                                           (Apply (Apply (Var "m") (Apply (Var "n") (Var "s"))) (Var "z"))))))
                                                                                       (Lambda ("s") (Lambda ("z") (Apply (Var "s") (Apply (Var "s") (Var "z"))))))
                                                                                   (Lambda ("s") (Lambda ("z") (Apply (Var "s") (Var "z")))))
                                                                               (Var "S"))
                                                                           (Var "Z"))
                                                                               @?= Apply (Var "S") (Apply (Var "S") (Var "Z")),
                                   testCase "eval (\\c . S c) (S Z) = S (S Z)"
                                                                   $ eval (Apply (Lambda ("c") (Apply (Var "S") (Var "c")))
                                                                                        (Apply (Var "S") (Var "Z")))
                                                                               @?= Apply (Var "S") (Apply (Var "S") (Var "Z")),
                                   testCase "eval (\\m . \\n . \\s . \\z . m (n s) z) (\\s . \\z . s (s z)) (\\s . \\z . s (s z)) S Z"
                                                                   $ eval (Apply
                                                                           (Apply (
                                                                               Apply (Apply (Lambda ("m")
                                                                                               (Lambda ("n")
                                                                                                   (Lambda ("s")
                                                                                                       (Lambda ("z")
                                                                                                           (Apply (Apply (Var "m") (Apply (Var "n") (Var "s"))) (Var "z"))))))
                                                                                       (Lambda ("s") (Lambda ("z") (Apply (Var "s") (Apply (Var "s") (Var "z"))))))
                                                                                   (Lambda ("s") (Lambda ("z") (Apply (Var "s") (Apply (Var "s") (Var "z"))))))
                                                                               (Var "S"))
                                                                           (Var "Z"))
                                                                               @?= Apply (Var "S") (Apply (Var "S") (Apply (Var "S") (Apply (Var "S") (Var "Z"))))
                             ]
          ]

