module AutoTests where

-- Do not change! It will be overwritten!
-- Use MyTests.hs to define your tests

import Test.Tasty (TestTree(..), testGroup, Timeout(..), localOption)
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck

import Lib

exampleTests :: [TestTree]
exampleTests =
  [ testGroup "HW 0"
    [ testGroup "HW 0.0"
      [ testCase "Works for Alice" $ hw0_0 "Alice" @?= "Hello, Alice"
      , testCase "Works for Bob"   $ hw0_0 "Bob"   @?= "Hello, Bob"
      ]
    ]
  , localOption (Timeout 1000000 "1s") $ testGroup "HW 1"
    [ testGroup "HW 1.1"
      [ testCase "Works for 2 and 3"    $ hw1_1 2 3    @?= 5
      , testCase "Works for 3 and 1000" $ hw1_1 3 1000 @?= 1003
      ]
    , testGroup "HW 1.2"
      [ testCase "Works on 2" $ hw1_2 2 @?= 1.25 ]
    , testGroup "n!!"
      [ testCase "3!!" $ fact2 3 @?= 3
      , testCase "4!!" $ fact2 4 @?= 8
      ]
    , testGroup "isPrime"
      [ testCase "isPrime 13" $ isPrime 13 @?= True
      , testCase "isPrime 12" $ isPrime 12 @?= False
      , testCase "isPrime 2"  $ isPrime 2  @?= True
      , testCase "isPrime 1"  $ isPrime 1  @?= False
      ]
    , testGroup "primeSum"
      [ testCase "primeSum 2 3" $ primeSum 2 3 @?= 5
      , testCase "primeSum 3 2" $ primeSum 3 2 @?= 0
      , testCase "primeSum 2 2" $ primeSum 2 2 @?= 2
      , testCase "primeSum 1 10" $ primeSum 1 10 @?= 17
      ]
    ]
  , localOption (Timeout 1000000 "1s") $ testGroup "HW 2"
    [ testGroup "isKnown"
      [ testCase "On"  $ isKnown On  @?= True
      , testCase "Off" $ isKnown Off @?= True
      , testCase "Unknown" $ isKnown Unknown @?= False
      ]
    , testGroup "eval"
      [ testCase "eval 2+3*(4+5)*(6-5)" $ eval_ (Add
                                                (Const 2)
                                                (Mult
                                                 (Const 3)
                                                 (Mult
                                                  (Add (Const 4) (Const 5))
                                                  (Sub (Const 6) (Const 5))))) @?= 29
      ]
    , testGroup "simplify"
      [ testCase "simplify 2+3*(4+5)*(6-5)" $ eval_ (simplify (Add (Const 2) (Mult (Const 3) (Mult (Add (Const 4) (Const 5)) (Sub (Const 6) (Const 5)))))) @?= 29
      ]
    ]
  , localOption (Timeout 1000000 "1s") $ testGroup "HW 3"
    [ testGroup "foldr"
      [ testCase "foldr works in simple case" $ foldr' (+) 0 [1,2,3,4,5] @?= 15
      , testCase "foldr wants (a -> b -> b)"  $ foldr' (\a b -> b && odd a) True [1,3,5] @?= True
      ]
    , testGroup "groupBy"
      [ testCase "groupBy (==)" $ groupBy' (==) [1,2,2,2,1,1,3] @?= [[1],[2,2,2],[1,1],[3]]
      ]
    , testGroup "either'"
      [ testCase "either' works with left function when Left"   $ either' length (+1) (Left' [10,11,12,13] :: Either' [Int] Int) @?= 4
      , testCase "either' works with right function when Right" $ either' length (+1) (Right' 2 :: Either' [Int] Int) @?= 3
      ]
    , testGroup "lefts'"
      [ testCase "lefts' return Lefts" $ lefts' [Left' 1, Right' [1,2], Left' 2, Right' [3,4]] @?= [1,2]
      , testCase "lefts' can return empty list" $ lefts' ([Right' [1,2], Right' [3,4]] :: [Either' Int [Int]]) @?= []
      ]
    , testGroup "rights'"
      [ testCase "rights' return Rights" $ rights' [Left' 1, Right' [1,2], Left' 2, Right' [3,4]] @?= [[1,2],[3,4]]
      , testCase "rights' can return empty list" $ rights' ([Left' 1, Left' 2] :: [Either' Int [Int]]) @?= []
      ]
    ]
  , localOption (Timeout 1000000 "1s") $ testGroup "HW 4"
    [ testGroup "Show'"
      [ testCase "show' A (-1)" $ show' (A (-1)) @?= "A (-1)"
      , testCase "show' C 1 2" $ show' (C 1 2) @?= "C 1 2"
      ]
    , testGroup "symmetricDifference {4,5,6} {5,6,7,8}" $ let
        s1 = Set $ \x -> x==4 || x==5 || x==6
        s2 = Set $ \x -> x==5 || x==6 || x==7 || x==8
        Set sd = symmetricDifference s1 s2
        in [ testCase "4 ∈" $ sd 4 @?= True
           , testCase "5 ∉" $ sd 5 @?= False
           , testCase "6 ∉" $ sd 6 @?= False
           , testCase "7 ∈" $ sd 7 @?= True
           , testCase "8 ∈" $ sd 8 @?= True ]
    , testGroup "fromBool"
      [ testCase "fromBool" $ fromBool True True False @?= True ]
    , testGroup "fromInt"
      [ testCase "fromInt" $ fromInt 10 (+1) 0 @?= 10 ]
    ]
  , localOption (Timeout 1000000 "1s") $ testGroup "HW 5"
    [ testGroup "Dyck Language"
      [ testCase "((())())" $ parse dyckLanguage "((())())" @?= Right ("((())())", "")
      , testCase "((())" $ either (const True) (const False) (parse dyckLanguage "((())") @?= True
      ]
    , testGroup "Arithmetic expressions"
      [ testCase "(2+(2*2))" $ parse arith "(2+(2*2))" @?= Right (Plus (Number 2) (Mul (Number 2) (Number 2)), "")
      , testCase "((1+2)-(3*4))" $ parse arith "((1+2)-(3*4))" @?= Right (Plus (Number 1) (Minus (Number 2) (Mul (Number 3) (Number 4))), "")
      ]
    , testGroup "Instances of Optional"
      [ testCase "fmap (+1) 1" $ fmap (+1) (Param 1 :: Optional Int) @?= Param 2
      , testCase "pure 1" $ (pure 1 :: Optional Int) @?= Param 1
      , testCase "+1 <*> 2" $ (pure (+1) <*> Param 2 :: Optional Int) @?= Param 3
      ]
    ]
  ]

oldTests :: [TestTree]
oldTests = [
  testGroup "HW 0"
    [ testGroup "HW 0.0"
      [ testCase "Works for Eve" $ hw0_0 "Eve" @?= "Hello, Eve"
      , testProperty "Works for any string" $ \s -> hw0_0 s === ("Hello, " ++ s)
      ]
    ]

  , localOption (Timeout 100000 "0.1s") $ testGroup "HW 1"
    [ testGroup "HW 1.1"
      [ testCase "Works for -1000 and 1000" $ hw1_1 (-1000) 1000 @?= 0
      , testProperty "Works for any a and b" $ \a b -> hw1_1 a b === a + b
      ]
    , testGroup "HW 1.2"
      [ testProperty "Works for any n" $ \n -> (n>0) ==> hw1_2 n `approx` sum (reverse [1.0 / fromIntegral (k^k) | k<-[1..n]])
      ]
    , testGroup "n!!"
      [ localOption (Timeout 1000000 "1s") $ testCase "0!!" $ fact2 0 @?= 1
      , testCase "1!!" $ fact2 1 @?= 1
      , localOption (Timeout 1000000 "1s") $ testProperty "Works for any n" $ \n -> (n>=0) ==> fact2 n === product (filter (\k -> odd k == odd n) [1..n])
      ]
    , testGroup "isPrime"
      [ testCase "isPrime 49"  $ isPrime 49 @?= False
      , testCase "isPrime 257" $ isPrime 257 @?= True
      , testCase "isPrime 32771*32769" $ isPrime (32771*32769) @?= False
      , testProperty "Works for any number" $ \n -> (n>=1) ==> isPrime n === myIsPrime n
      , testProperty "Works for any product" $ \a b -> (a>1 && b>1) ==> isPrime (a*b) === False
      ]
    , testGroup "isPrime is not slow"
      [ localOption (Timeout 10000000 "10s") $ testCase "isPrime 10^10 + 19" $ isPrime 10000000019 @?= True
      ]
    , testGroup "primeSum"
      [ testProperty "Works for any interval" $ \a b -> (a>0 && b>0) ==> primeSum a b === sum (filter myIsPrime [a..b])
      ]
    ]
  ]
  where approx :: Double -> Double -> Bool
        approx x1 x2 = abs (x1 - x2) < 0.000001
        
        myIsPrime :: Integer -> Bool
        myIsPrime 1 = False
        myIsPrime n = all (\p->n`mod`p/=0) $ takeWhile (\i->i*i<=n) $ 2:[3,5..n]

autoTests :: [TestTree]
autoTests = [ ]

