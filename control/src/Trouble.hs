{-==== Мишин Геннадий, 11-302 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
rocket xs = if snd maxDiv == minimum (map snd xs)
            then [maxDiv]
            else xs
    where maxDiv = getMaxDivision xs
          getMaxDivision (x:xs) = foldl (\(a,b) (c,d) -> if (a `div` b) > (c `div` d) then (a,b) else (c,d)) x xs

{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
orbiters :: [Spaceship a] -> [Load a]
orbiters [] = []
orbiters sps = concatMap singleCargo sps
singleCargo :: Spaceship a -> [Load a]
singleCargo (Cargo cargo) = filter isOrbiter cargo
    where isOrbiter (Orbiter _) = True
          isOrbiter _ = False
singleCargo (Rocket h sps) = orbiters sps

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
calcPhrase :: Phrase -> String
calcPhrase phrase = case phrase of
        (Warp _) -> "Kirk"
        (BeamUp _) -> "Kirk"
        (IsDead _) -> "McCoy"
        (LiveLongAndProsper) -> "Spock"
        (Fascinating) -> "Spock"

finalFrontier :: [Phrase] -> [String]
finalFrontier = map calcPhrase



